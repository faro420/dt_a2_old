--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Code belongs to TI3 DT
-- Stimuli Generator for ISE resp. XC2C256-CPLD related timing simulation just an example
--
-- VCS: git@BitBucket.org/schaefers/DT-DEMO-SG4CPLD.git
-- PUB: https://pub.informatik.haw-hamburg.de/home/pub/prof/schaefers_michael/*_DT/_CODE_/...
--
-- HISTORY:
-- ========
-- WS13/14
--      release for TI3 DT WS13/14  by Michael Schaefers
-- WS14/15 (141104):
--      update for TI3 DT WS14/15  by Michael Schaefers 
------------------------------------------------------------------------------
-- BEMERKUNG:
-- ==========
-- Deutscher Kommentar fuer Erklaerungen fuer speziell Studenten/VHDL-Anfaenger - dieser Kommentar wuerde im "Normalfall" fehlen
-- Englischer Kommentar als "normaler" Kommentar



library work;
    use work.all;

library std;
    use std.textio.all;                 -- benoetigt vom "markStart"-PROCESS

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")
    use ieee.std_logic_unsigned.all;    -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")



-- sg ::= Stimuli Generator    
entity sg is
    port (
        data : out std_logic_vector( 3 downto 0);
        clk  : out std_logic;
        nres : out std_logic
    );--]port
end entity sg;



architecture beh of sg is
    
    -- Das Arbeiten mit 1/8 Takt-Zyklen ist eigentlich "uebertrieben",
    --   aber es erleichtert das Lesen der Waves
    --   und es bleibt auf den Stimuli Generator beschraenkt
    -- 8*2500ps = 20ns Takt-Periode => 50MHz Takt-Frequenz
    constant oneEigthClockCycle     : time  := 2500 ps;
    
    signal   nres_s                 : std_logic  := 'L';    -- "internal" nres - workaround to enable reading nres without using "buffer-ports"
    signal   clk_s                  : std_logic;            -- "internal" clk - workaround to enable reading clk without using "buffer-ports"
	
	signal   simulationRunning_s    : boolean  := true;     -- for internal signalling that simulation is running
    
begin

    resGen:                                                 -- RESet GENerator
    process is
    begin
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear low active reset
        -- 1/8 clk period after rising clk edge reset is vanishing
        
        wait;
    end process resGen;
    --
    nres <= nres_s;
    
    clkgen:
        process is
        begin
            while simulationRunning_s loop
                clk_s <= '0';
                wait for 500 ns;
                clk_s <= '1';
                wait for 500 ns;
            end loop; -- unn�tig(2), aber u.U. besser lesbar
        end process clkgen;
    --
    clk <= clk_s;
    
    sg:                                                     -- Stimuli Generator
    process is
		variable data_v : std_logic_vector(3 downto 0);
    begin
        
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        data <= (others=>'0');                              -- just set any defined data
        if  nres_s/='1'  then  wait until nres_s='1';  end if;
        --reset has passed
        
        
        
        -- give CPLD some time to wake up
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- CPLD is configured and able to react

		data_v := "0000"; 
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0001";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0010";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0011";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0100";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0101";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0110";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "0111"; 
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1000"; 
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1001";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1010";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1011"; 
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
        data_v := "1100";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1101";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1110";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
		data_v := "1111";
        data <= data_v; wait until '1'=clk_s and clk_s'event;
		
        -- stop SG after 10 clk cycles - assuming computed data is stable afterwards
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        simulationRunning_s <= false;                       -- stop clk generation
        --
        wait;
    end process sg;
    
end architecture beh;
