--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Testbench for dut
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library work;
use work.all;

entity tbDut is
end tbDut;

architecture beh of tbDut is
    signal data_s : std_logic_vector(3 downto 0);
    signal clk_s : std_logic;
    signal nres_s : std_logic;
    signal rxdat_s : std_logic_vector(3 downto 0);
    signal ok_s : std_logic;

    component sg is
        port (
            data : out std_logic_vector( 3 downto 0);
            clk  : out std_logic;
            nres : out std_logic
        );
    end component sg;
    for all : sg use entity work.sg( beh );

    component dut is
	    port(
            rxdat :out std_logic_vector(3 downto 0);
            ok: out std_logic;
            txdat :in std_logic_vector(3 downto 0);
            clk: in std_logic;
            nres: in std_logic
        );
    end component;
    for all: dut use entity work.dut( Structure );

begin
	sg_i : sg
        port map (
            data => data_s,
            clk => clk_s,
            nres => nres_s
            
        );
    
    dut_i : dut
        port map (
            rxdat => rxdat_s,
            clk => clk_s,
            nres => nres_s,
            txdat => data_s,
            ok => ok_s
        );
end beh;