--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library work;
use work.all;

entity dut is
    port(
        rxdat :out std_logic_vector(3 downto 0);
        ok: out std_logic;
        txdat :in std_logic_vector(3 downto 0);
        clk: in std_logic;
        nres: in std_logic
    );
end dut;

architecture beh of dut is
    component parGen is
    port(
       txdat :in std_logic_vector(3 downto 0);
       txdat_secured :out std_logic_vector(4 downto 0);
       clk: in std_logic;
       nres: in std_logic
    );
    end component parGen;
    for all : parGen use entity work.parGen(beh);
    
    component parCheck is
    port(
        txdat_secured :in std_logic_vector(4 downto 0);
        rxdat :out std_logic_vector(3 downto 0);
        ok: out std_logic;
        clk: in std_logic;
        nres: in std_logic
    );
    end component parCheck;
    for all : parCheck use entity work.parCheck(beh);
    
    signal bridge : std_logic_vector(4 downto 0);
begin
    pGen : parGen
        port map (
            txdat => txdat,
            txdat_secured => bridge,
            clk => clk,
            nres => nres
        );
    
    pCheck : parCheck
        port map (
            txdat_secured => bridge,
            rxdat => rxdat,
            ok => ok,
            clk => clk,
            nres => nres
        );
        
end beh;