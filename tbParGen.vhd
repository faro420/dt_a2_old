--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

-- Testbench for parGen
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library work;
use work.all;

entity tbParGen is
end tbParGen;

architecture beh of tbParGen is
    signal data_s : std_logic_vector(3 downto 0);
    signal clk_s : std_logic;
    signal nres_s : std_logic;
    signal txdat_s : std_logic_vector(4 downto 0);

    component sg is
        port (
            data : out std_logic_vector( 3 downto 0);
            clk  : out std_logic;
            nres : out std_logic
        );
    end component sg;
    for all : sg use entity work.sg( beh );

    component parGen is
	    port(
            txdat :in std_logic_vector(3 downto 0);
            txdat_secured :out std_logic_vector(4 downto 0);
            clk: in std_logic;
            nres: in std_logic
        );
    end component;
    for all: parGen use entity work.parGen ( beh );

begin
	sg_i : sg
        port map (
            data => data_s,
            clk => clk_s,
            nres => nres_s
        );
    
    dut_i : parGen
        port map (
            txdat => data_s,
            txdat_secured => txdat_s,
            clk => clk_s,
            nres => nres_s
        );
end beh;