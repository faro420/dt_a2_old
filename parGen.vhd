--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

--VHDL code for even parity check
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity parGen is
    port(
       txdat :in std_logic_vector(3 downto 0);
       txdat_secured :out std_logic_vector(4 downto 0);
       clk: in std_logic;
       nres: in std_logic
    );
end entity parGen;

architecture beh of parGen is
    signal r1_cs : std_logic_vector(3 downto 0) := (others => '0');
    signal r2_cs : std_logic_vector(4 downto 0) := (others => '0');
    signal r2_ns : std_logic_vector(4 downto 0);
begin   
    pgen:    
    process (r1_cs) is
        variable r1_v : std_logic_vector(3 downto 0);
        variable resu_v : std_logic_vector(4 downto 0);
        
    begin
        r1_v  := r1_cs;
        resu_v(resu_v'high) := not ( r1_v(3) XOR r1_v(2) XOR r1_v(1) XOR r1_v(0) );
        resu_v(r1_v'high downto 0) := r1_v(r1_v'high downto 0);
        r2_ns <= resu_v;
    end process pgen;
    
    
  --r1_ns <= txdat;
    --
    regGen:
    process ( clk ) is
    begin
    if clk = '1' and clk'event then
        if nres = '0' then
            r1_cs <= ( others => '0' );
            r2_cs <= ( others => '0' );
        else
            r1_cs <= txdat;
            r2_cs <= r2_ns;
        end if;    
    end if;
    end process regGen;
    --
    txdat_secured <= r2_cs;
    
end architecture beh;