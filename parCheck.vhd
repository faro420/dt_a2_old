--@authors: Mir Farshid Baha,2141801, mirfarshid.baha@haw-hamburg.de
--          Mehmet Cakir, 2195657, mehmet.cakir@haw-hamburg.de

--VHDL code for even parity check
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

entity parCheck is
    port(
        txdat_secured :in std_logic_vector(4 downto 0);
        rxdat :out std_logic_vector(3 downto 0);
        ok: out std_logic;
        clk: in std_logic;
        nres: in std_logic
    );
end entity parCheck;

architecture beh of parCheck is
    signal r1_cs : std_logic_vector(4 downto 0) := (others => '0');
    signal r2_cs : std_logic_vector(3 downto 0) := (others => '0');
    signal r2_ns : std_logic_vector(3 downto 0);
    signal ok_cs : std_logic := '0';
    signal ok_ns : std_logic;
begin
    process(r1_cs) is
        variable r1_v  : std_logic_vector(4 downto 0);
        variable resu_v : std_logic_vector(3 downto 0);
        variable ok_v: std_logic;
    begin
        r1_v  := r1_cs;
        ok_v :=  r1_v(4) XOR r1_v(3) XOR r1_v(2) XOR r1_v(1) XOR r1_v(0);
        resu_v((r1_v'high-1) downto 0) := r1_v((r1_v'high-1) downto 0);
        r2_ns <= resu_v;
        ok_ns <= ok_v;
    end process;
    
    regCheck:
    process ( clk ) is
    begin
    if clk = '1' and clk'event then
        if nres = '0' then
            r1_cs <= ( others => '0' );
            r2_cs <= ( others => '0' );
            ok_cs <= '0';
        else
            r1_cs <= txdat_secured;
            r2_cs <= r2_ns;
            ok_cs <= ok_ns;
        end if;    
    end if;
    end process regCheck;
    --
    rxdat <= r2_cs;
    ok <= ok_cs;
end architecture beh;