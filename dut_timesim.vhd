--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: dut_timesim.vhd
-- /___/   /\     Timestamp: Fri Nov 27 13:40:00 2015
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm dut -w -dir netgen/fit -ofmt vhdl -sim dut.nga dut_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: dut.nga
-- Output file	: D:\ISE_Projects\transmitter\netgen\fit\dut_timesim.vhd
-- # of Entities	: 1
-- Design Name	: dut.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

-- entity dut is
  -- port (
    -- clk : in STD_LOGIC := 'X'; 
    -- nres : in STD_LOGIC := 'X'; 
    -- ok : out STD_LOGIC; 
    -- txdat : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
    -- rxdat : out STD_LOGIC_VECTOR ( 3 downto 0 ) 
  -- );
-- end dut;

architecture Structure of dut is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal txdat_0_II_UIM_5 : STD_LOGIC; 
  signal txdat_1_II_UIM_7 : STD_LOGIC; 
  signal txdat_2_II_UIM_9 : STD_LOGIC; 
  signal txdat_3_II_UIM_11 : STD_LOGIC; 
  signal ok_MC_Q_13 : STD_LOGIC; 
  signal rxdat_0_MC_Q_15 : STD_LOGIC; 
  signal rxdat_1_MC_Q_17 : STD_LOGIC; 
  signal rxdat_2_MC_Q_19 : STD_LOGIC; 
  signal rxdat_3_MC_Q_21 : STD_LOGIC; 
  signal ok_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal ok_MC_D_23 : STD_LOGIC; 
  signal Gnd_24 : STD_LOGIC; 
  signal Vcc_25 : STD_LOGIC; 
  signal ok_MC_D1_26 : STD_LOGIC; 
  signal ok_MC_D2_27 : STD_LOGIC; 
  signal ok_MC_D2_PT_0_33 : STD_LOGIC; 
  signal ok_MC_D2_PT_1_34 : STD_LOGIC; 
  signal ok_MC_D2_PT_2_35 : STD_LOGIC; 
  signal ok_MC_D2_PT_3_36 : STD_LOGIC; 
  signal ok_MC_D2_PT_4_37 : STD_LOGIC; 
  signal ok_MC_D2_PT_5_38 : STD_LOGIC; 
  signal ok_MC_D2_PT_6_39 : STD_LOGIC; 
  signal ok_MC_D2_PT_7_40 : STD_LOGIC; 
  signal pCheck_r1_cs_0_MC_Q : STD_LOGIC; 
  signal pCheck_r1_cs_0_MC_D_42 : STD_LOGIC; 
  signal pCheck_r1_cs_0_MC_D1_43 : STD_LOGIC; 
  signal pCheck_r1_cs_0_MC_D2_44 : STD_LOGIC; 
  signal bridge_0_MC_Q : STD_LOGIC; 
  signal bridge_0_MC_D_47 : STD_LOGIC; 
  signal bridge_0_MC_D1_48 : STD_LOGIC; 
  signal bridge_0_MC_D2_49 : STD_LOGIC; 
  signal pGen_r1_cs_0_MC_Q : STD_LOGIC; 
  signal pGen_r1_cs_0_MC_D_52 : STD_LOGIC; 
  signal pGen_r1_cs_0_MC_D1_53 : STD_LOGIC; 
  signal pGen_r1_cs_0_MC_D2_54 : STD_LOGIC; 
  signal pCheck_r1_cs_1_MC_Q : STD_LOGIC; 
  signal pCheck_r1_cs_1_MC_D_56 : STD_LOGIC; 
  signal pCheck_r1_cs_1_MC_D1_57 : STD_LOGIC; 
  signal pCheck_r1_cs_1_MC_D2_58 : STD_LOGIC; 
  signal bridge_1_MC_Q : STD_LOGIC; 
  signal bridge_1_MC_D_61 : STD_LOGIC; 
  signal bridge_1_MC_D1_62 : STD_LOGIC; 
  signal bridge_1_MC_D2_63 : STD_LOGIC; 
  signal pGen_r1_cs_1_MC_Q : STD_LOGIC; 
  signal pGen_r1_cs_1_MC_D_66 : STD_LOGIC; 
  signal pGen_r1_cs_1_MC_D1_67 : STD_LOGIC; 
  signal pGen_r1_cs_1_MC_D2_68 : STD_LOGIC; 
  signal pCheck_r1_cs_2_MC_Q : STD_LOGIC; 
  signal pCheck_r1_cs_2_MC_D_70 : STD_LOGIC; 
  signal pCheck_r1_cs_2_MC_D1_71 : STD_LOGIC; 
  signal pCheck_r1_cs_2_MC_D2_72 : STD_LOGIC; 
  signal bridge_2_MC_Q : STD_LOGIC; 
  signal bridge_2_MC_D_75 : STD_LOGIC; 
  signal bridge_2_MC_D1_76 : STD_LOGIC; 
  signal bridge_2_MC_D2_77 : STD_LOGIC; 
  signal pGen_r1_cs_2_MC_Q : STD_LOGIC; 
  signal pGen_r1_cs_2_MC_D_80 : STD_LOGIC; 
  signal pGen_r1_cs_2_MC_D1_81 : STD_LOGIC; 
  signal pGen_r1_cs_2_MC_D2_82 : STD_LOGIC; 
  signal pCheck_r1_cs_3_MC_Q : STD_LOGIC; 
  signal pCheck_r1_cs_3_MC_D_84 : STD_LOGIC; 
  signal pCheck_r1_cs_3_MC_D1_85 : STD_LOGIC; 
  signal pCheck_r1_cs_3_MC_D2_86 : STD_LOGIC; 
  signal bridge_3_MC_Q : STD_LOGIC; 
  signal bridge_3_MC_D_89 : STD_LOGIC; 
  signal bridge_3_MC_D1_90 : STD_LOGIC; 
  signal bridge_3_MC_D2_91 : STD_LOGIC; 
  signal pGen_r1_cs_3_MC_Q : STD_LOGIC; 
  signal pGen_r1_cs_3_MC_D_94 : STD_LOGIC; 
  signal pGen_r1_cs_3_MC_D1_95 : STD_LOGIC; 
  signal pGen_r1_cs_3_MC_D2_96 : STD_LOGIC; 
  signal pCheck_r1_cs_4_MC_Q : STD_LOGIC; 
  signal pCheck_r1_cs_4_MC_D_98 : STD_LOGIC; 
  signal pCheck_r1_cs_4_MC_D1_99 : STD_LOGIC; 
  signal pCheck_r1_cs_4_MC_D2_100 : STD_LOGIC; 
  signal bridge_4_MC_Q : STD_LOGIC; 
  signal bridge_4_MC_D_103 : STD_LOGIC; 
  signal bridge_4_MC_D1_104 : STD_LOGIC; 
  signal bridge_4_MC_D2_105 : STD_LOGIC; 
  signal bridge_4_MC_D2_PT_0_106 : STD_LOGIC; 
  signal bridge_4_MC_D2_PT_1_107 : STD_LOGIC; 
  signal bridge_4_MC_D2_PT_2_108 : STD_LOGIC; 
  signal bridge_4_MC_D2_PT_3_109 : STD_LOGIC; 
  signal rxdat_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal rxdat_0_MC_D_111 : STD_LOGIC; 
  signal rxdat_0_MC_D1_112 : STD_LOGIC; 
  signal rxdat_0_MC_D2_113 : STD_LOGIC; 
  signal rxdat_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal rxdat_1_MC_D_115 : STD_LOGIC; 
  signal rxdat_1_MC_D1_116 : STD_LOGIC; 
  signal rxdat_1_MC_D2_117 : STD_LOGIC; 
  signal rxdat_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal rxdat_2_MC_D_119 : STD_LOGIC; 
  signal rxdat_2_MC_D1_120 : STD_LOGIC; 
  signal rxdat_2_MC_D2_121 : STD_LOGIC; 
  signal rxdat_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal rxdat_3_MC_D_123 : STD_LOGIC; 
  signal rxdat_3_MC_D1_124 : STD_LOGIC; 
  signal rxdat_3_MC_D2_125 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_ok_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_bridge_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_bridge_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_bridge_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_bridge_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_bridge_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_bridge_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_bridge_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_bridge_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pGen_r1_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_pCheck_r1_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_bridge_4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_rxdat_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_ok_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_bridge_4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_bridge_4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_bridge_4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_bridge_4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_bridge_4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_bridge_4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal pCheck_r1_cs : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal bridge : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal pGen_r1_cs : STD_LOGIC_VECTOR ( 3 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  txdat_0_II_UIM : X_BUF
    port map (
      I => txdat(0),
      O => txdat_0_II_UIM_5
    );
  txdat_1_II_UIM : X_BUF
    port map (
      I => txdat(1),
      O => txdat_1_II_UIM_7
    );
  txdat_2_II_UIM : X_BUF
    port map (
      I => txdat(2),
      O => txdat_2_II_UIM_9
    );
  txdat_3_II_UIM : X_BUF
    port map (
      I => txdat(3),
      O => txdat_3_II_UIM_11
    );
  ok_14 : X_BUF
    port map (
      I => ok_MC_Q_13,
      O => ok
    );
  rxdat_0_Q : X_BUF
    port map (
      I => rxdat_0_MC_Q_15,
      O => rxdat(0)
    );
  rxdat_1_Q : X_BUF
    port map (
      I => rxdat_1_MC_Q_17,
      O => rxdat(1)
    );
  rxdat_2_Q : X_BUF
    port map (
      I => rxdat_2_MC_Q_19,
      O => rxdat(2)
    );
  rxdat_3_Q : X_BUF
    port map (
      I => rxdat_3_MC_Q_21,
      O => rxdat(3)
    );
  ok_MC_Q : X_BUF
    port map (
      I => ok_MC_Q_tsimrenamed_net_Q,
      O => ok_MC_Q_13
    );
  ok_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_ok_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_ok_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => ok_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_24
    );
  Vcc : X_ONE
    port map (
      O => Vcc_25
    );
  ok_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_ok_MC_D_IN0,
      I1 => NlwBufferSignal_ok_MC_D_IN1,
      O => ok_MC_D_23
    );
  ok_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_ok_MC_D1_IN0,
      I1 => NlwBufferSignal_ok_MC_D1_IN1,
      O => ok_MC_D1_26
    );
  ok_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_ok_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_ok_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_ok_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_ok_MC_D2_PT_0_IN4,
      O => ok_MC_D2_PT_0_33
    );
  ok_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_ok_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_ok_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_ok_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_ok_MC_D2_PT_1_IN4,
      O => ok_MC_D2_PT_1_34
    );
  ok_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_ok_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_ok_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_ok_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_ok_MC_D2_PT_2_IN4,
      O => ok_MC_D2_PT_2_35
    );
  ok_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_ok_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_ok_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_ok_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_ok_MC_D2_PT_3_IN4,
      O => ok_MC_D2_PT_3_36
    );
  ok_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_ok_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_ok_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_ok_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_ok_MC_D2_PT_4_IN4,
      O => ok_MC_D2_PT_4_37
    );
  ok_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_ok_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_ok_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_ok_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_ok_MC_D2_PT_5_IN4,
      O => ok_MC_D2_PT_5_38
    );
  ok_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_ok_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_ok_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_ok_MC_D2_PT_6_IN3,
      I4 => NlwInverterSignal_ok_MC_D2_PT_6_IN4,
      O => ok_MC_D2_PT_6_39
    );
  ok_MC_D2_PT_7 : X_AND5
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_ok_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_ok_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_ok_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_ok_MC_D2_PT_7_IN4,
      O => ok_MC_D2_PT_7_40
    );
  ok_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_ok_MC_D2_IN0,
      I1 => NlwBufferSignal_ok_MC_D2_IN1,
      I2 => NlwBufferSignal_ok_MC_D2_IN2,
      I3 => NlwBufferSignal_ok_MC_D2_IN3,
      I4 => NlwBufferSignal_ok_MC_D2_IN4,
      I5 => NlwBufferSignal_ok_MC_D2_IN5,
      I6 => NlwBufferSignal_ok_MC_D2_IN6,
      I7 => NlwBufferSignal_ok_MC_D2_IN7,
      O => ok_MC_D2_27
    );
  pCheck_r1_cs_0_Q : X_BUF
    port map (
      I => pCheck_r1_cs_0_MC_Q,
      O => pCheck_r1_cs(0)
    );
  pCheck_r1_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pCheck_r1_cs_0_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pCheck_r1_cs_0_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pCheck_r1_cs_0_MC_Q
    );
  pCheck_r1_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_0_MC_D_IN1,
      O => pCheck_r1_cs_0_MC_D_42
    );
  pCheck_r1_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_0_MC_D1_IN1,
      O => pCheck_r1_cs_0_MC_D1_43
    );
  pCheck_r1_cs_0_MC_D2 : X_ZERO
    port map (
      O => pCheck_r1_cs_0_MC_D2_44
    );
  bridge_0_Q : X_BUF
    port map (
      I => bridge_0_MC_Q,
      O => bridge(0)
    );
  bridge_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_bridge_0_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_bridge_0_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => bridge_0_MC_Q
    );
  bridge_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_bridge_0_MC_D_IN0,
      I1 => NlwBufferSignal_bridge_0_MC_D_IN1,
      O => bridge_0_MC_D_47
    );
  bridge_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_bridge_0_MC_D1_IN0,
      I1 => NlwBufferSignal_bridge_0_MC_D1_IN1,
      O => bridge_0_MC_D1_48
    );
  bridge_0_MC_D2 : X_ZERO
    port map (
      O => bridge_0_MC_D2_49
    );
  pGen_r1_cs_0_Q : X_BUF
    port map (
      I => pGen_r1_cs_0_MC_Q,
      O => pGen_r1_cs(0)
    );
  pGen_r1_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pGen_r1_cs_0_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pGen_r1_cs_0_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pGen_r1_cs_0_MC_Q
    );
  pGen_r1_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_0_MC_D_IN1,
      O => pGen_r1_cs_0_MC_D_52
    );
  pGen_r1_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_0_MC_D1_IN1,
      O => pGen_r1_cs_0_MC_D1_53
    );
  pGen_r1_cs_0_MC_D2 : X_ZERO
    port map (
      O => pGen_r1_cs_0_MC_D2_54
    );
  pCheck_r1_cs_1_Q : X_BUF
    port map (
      I => pCheck_r1_cs_1_MC_Q,
      O => pCheck_r1_cs(1)
    );
  pCheck_r1_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pCheck_r1_cs_1_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pCheck_r1_cs_1_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pCheck_r1_cs_1_MC_Q
    );
  pCheck_r1_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_1_MC_D_IN1,
      O => pCheck_r1_cs_1_MC_D_56
    );
  pCheck_r1_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_1_MC_D1_IN1,
      O => pCheck_r1_cs_1_MC_D1_57
    );
  pCheck_r1_cs_1_MC_D2 : X_ZERO
    port map (
      O => pCheck_r1_cs_1_MC_D2_58
    );
  bridge_1_Q : X_BUF
    port map (
      I => bridge_1_MC_Q,
      O => bridge(1)
    );
  bridge_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_bridge_1_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_bridge_1_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => bridge_1_MC_Q
    );
  bridge_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_bridge_1_MC_D_IN0,
      I1 => NlwBufferSignal_bridge_1_MC_D_IN1,
      O => bridge_1_MC_D_61
    );
  bridge_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_bridge_1_MC_D1_IN0,
      I1 => NlwBufferSignal_bridge_1_MC_D1_IN1,
      O => bridge_1_MC_D1_62
    );
  bridge_1_MC_D2 : X_ZERO
    port map (
      O => bridge_1_MC_D2_63
    );
  pGen_r1_cs_1_Q : X_BUF
    port map (
      I => pGen_r1_cs_1_MC_Q,
      O => pGen_r1_cs(1)
    );
  pGen_r1_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pGen_r1_cs_1_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pGen_r1_cs_1_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pGen_r1_cs_1_MC_Q
    );
  pGen_r1_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_1_MC_D_IN1,
      O => pGen_r1_cs_1_MC_D_66
    );
  pGen_r1_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_1_MC_D1_IN1,
      O => pGen_r1_cs_1_MC_D1_67
    );
  pGen_r1_cs_1_MC_D2 : X_ZERO
    port map (
      O => pGen_r1_cs_1_MC_D2_68
    );
  pCheck_r1_cs_2_Q : X_BUF
    port map (
      I => pCheck_r1_cs_2_MC_Q,
      O => pCheck_r1_cs(2)
    );
  pCheck_r1_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pCheck_r1_cs_2_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pCheck_r1_cs_2_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pCheck_r1_cs_2_MC_Q
    );
  pCheck_r1_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_2_MC_D_IN1,
      O => pCheck_r1_cs_2_MC_D_70
    );
  pCheck_r1_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_2_MC_D1_IN1,
      O => pCheck_r1_cs_2_MC_D1_71
    );
  pCheck_r1_cs_2_MC_D2 : X_ZERO
    port map (
      O => pCheck_r1_cs_2_MC_D2_72
    );
  bridge_2_Q : X_BUF
    port map (
      I => bridge_2_MC_Q,
      O => bridge(2)
    );
  bridge_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_bridge_2_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_bridge_2_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => bridge_2_MC_Q
    );
  bridge_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_bridge_2_MC_D_IN0,
      I1 => NlwBufferSignal_bridge_2_MC_D_IN1,
      O => bridge_2_MC_D_75
    );
  bridge_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_bridge_2_MC_D1_IN0,
      I1 => NlwBufferSignal_bridge_2_MC_D1_IN1,
      O => bridge_2_MC_D1_76
    );
  bridge_2_MC_D2 : X_ZERO
    port map (
      O => bridge_2_MC_D2_77
    );
  pGen_r1_cs_2_Q : X_BUF
    port map (
      I => pGen_r1_cs_2_MC_Q,
      O => pGen_r1_cs(2)
    );
  pGen_r1_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pGen_r1_cs_2_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pGen_r1_cs_2_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pGen_r1_cs_2_MC_Q
    );
  pGen_r1_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_2_MC_D_IN1,
      O => pGen_r1_cs_2_MC_D_80
    );
  pGen_r1_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_2_MC_D1_IN1,
      O => pGen_r1_cs_2_MC_D1_81
    );
  pGen_r1_cs_2_MC_D2 : X_ZERO
    port map (
      O => pGen_r1_cs_2_MC_D2_82
    );
  pCheck_r1_cs_3_Q : X_BUF
    port map (
      I => pCheck_r1_cs_3_MC_Q,
      O => pCheck_r1_cs(3)
    );
  pCheck_r1_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pCheck_r1_cs_3_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pCheck_r1_cs_3_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pCheck_r1_cs_3_MC_Q
    );
  pCheck_r1_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_3_MC_D_IN1,
      O => pCheck_r1_cs_3_MC_D_84
    );
  pCheck_r1_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_3_MC_D1_IN1,
      O => pCheck_r1_cs_3_MC_D1_85
    );
  pCheck_r1_cs_3_MC_D2 : X_ZERO
    port map (
      O => pCheck_r1_cs_3_MC_D2_86
    );
  bridge_3_Q : X_BUF
    port map (
      I => bridge_3_MC_Q,
      O => bridge(3)
    );
  bridge_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_bridge_3_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_bridge_3_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => bridge_3_MC_Q
    );
  bridge_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_bridge_3_MC_D_IN0,
      I1 => NlwBufferSignal_bridge_3_MC_D_IN1,
      O => bridge_3_MC_D_89
    );
  bridge_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_bridge_3_MC_D1_IN0,
      I1 => NlwBufferSignal_bridge_3_MC_D1_IN1,
      O => bridge_3_MC_D1_90
    );
  bridge_3_MC_D2 : X_ZERO
    port map (
      O => bridge_3_MC_D2_91
    );
  pGen_r1_cs_3_Q : X_BUF
    port map (
      I => pGen_r1_cs_3_MC_Q,
      O => pGen_r1_cs(3)
    );
  pGen_r1_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pGen_r1_cs_3_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pGen_r1_cs_3_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pGen_r1_cs_3_MC_Q
    );
  pGen_r1_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_3_MC_D_IN1,
      O => pGen_r1_cs_3_MC_D_94
    );
  pGen_r1_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pGen_r1_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_pGen_r1_cs_3_MC_D1_IN1,
      O => pGen_r1_cs_3_MC_D1_95
    );
  pGen_r1_cs_3_MC_D2 : X_ZERO
    port map (
      O => pGen_r1_cs_3_MC_D2_96
    );
  pCheck_r1_cs_4_Q : X_BUF
    port map (
      I => pCheck_r1_cs_4_MC_Q,
      O => pCheck_r1_cs(4)
    );
  pCheck_r1_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_pCheck_r1_cs_4_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_pCheck_r1_cs_4_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => pCheck_r1_cs_4_MC_Q
    );
  pCheck_r1_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_4_MC_D_IN1,
      O => pCheck_r1_cs_4_MC_D_98
    );
  pCheck_r1_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_pCheck_r1_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_pCheck_r1_cs_4_MC_D1_IN1,
      O => pCheck_r1_cs_4_MC_D1_99
    );
  pCheck_r1_cs_4_MC_D2 : X_ZERO
    port map (
      O => pCheck_r1_cs_4_MC_D2_100
    );
  bridge_4_Q : X_BUF
    port map (
      I => bridge_4_MC_Q,
      O => bridge(4)
    );
  bridge_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_bridge_4_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_bridge_4_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => bridge_4_MC_Q
    );
  bridge_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D_IN0,
      I1 => NlwBufferSignal_bridge_4_MC_D_IN1,
      O => bridge_4_MC_D_103
    );
  bridge_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D1_IN0,
      I1 => NlwBufferSignal_bridge_4_MC_D1_IN1,
      O => bridge_4_MC_D1_104
    );
  bridge_4_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_bridge_4_MC_D2_PT_0_IN3,
      O => bridge_4_MC_D2_PT_0_106
    );
  bridge_4_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_bridge_4_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN3,
      O => bridge_4_MC_D2_PT_1_107
    );
  bridge_4_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_bridge_4_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN3,
      O => bridge_4_MC_D2_PT_2_108
    );
  bridge_4_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_bridge_4_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_bridge_4_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_bridge_4_MC_D2_PT_3_IN3,
      O => bridge_4_MC_D2_PT_3_109
    );
  bridge_4_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_bridge_4_MC_D2_IN0,
      I1 => NlwBufferSignal_bridge_4_MC_D2_IN1,
      I2 => NlwBufferSignal_bridge_4_MC_D2_IN2,
      I3 => NlwBufferSignal_bridge_4_MC_D2_IN3,
      O => bridge_4_MC_D2_105
    );
  rxdat_0_MC_Q : X_BUF
    port map (
      I => rxdat_0_MC_Q_tsimrenamed_net_Q,
      O => rxdat_0_MC_Q_15
    );
  rxdat_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_rxdat_0_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_rxdat_0_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => rxdat_0_MC_Q_tsimrenamed_net_Q
    );
  rxdat_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_rxdat_0_MC_D_IN0,
      I1 => NlwBufferSignal_rxdat_0_MC_D_IN1,
      O => rxdat_0_MC_D_111
    );
  rxdat_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_rxdat_0_MC_D1_IN0,
      I1 => NlwBufferSignal_rxdat_0_MC_D1_IN1,
      O => rxdat_0_MC_D1_112
    );
  rxdat_0_MC_D2 : X_ZERO
    port map (
      O => rxdat_0_MC_D2_113
    );
  rxdat_1_MC_Q : X_BUF
    port map (
      I => rxdat_1_MC_Q_tsimrenamed_net_Q,
      O => rxdat_1_MC_Q_17
    );
  rxdat_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_rxdat_1_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_rxdat_1_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => rxdat_1_MC_Q_tsimrenamed_net_Q
    );
  rxdat_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_rxdat_1_MC_D_IN0,
      I1 => NlwBufferSignal_rxdat_1_MC_D_IN1,
      O => rxdat_1_MC_D_115
    );
  rxdat_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_rxdat_1_MC_D1_IN0,
      I1 => NlwBufferSignal_rxdat_1_MC_D1_IN1,
      O => rxdat_1_MC_D1_116
    );
  rxdat_1_MC_D2 : X_ZERO
    port map (
      O => rxdat_1_MC_D2_117
    );
  rxdat_2_MC_Q : X_BUF
    port map (
      I => rxdat_2_MC_Q_tsimrenamed_net_Q,
      O => rxdat_2_MC_Q_19
    );
  rxdat_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_rxdat_2_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_rxdat_2_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => rxdat_2_MC_Q_tsimrenamed_net_Q
    );
  rxdat_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_rxdat_2_MC_D_IN0,
      I1 => NlwBufferSignal_rxdat_2_MC_D_IN1,
      O => rxdat_2_MC_D_119
    );
  rxdat_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_rxdat_2_MC_D1_IN0,
      I1 => NlwBufferSignal_rxdat_2_MC_D1_IN1,
      O => rxdat_2_MC_D1_120
    );
  rxdat_2_MC_D2 : X_ZERO
    port map (
      O => rxdat_2_MC_D2_121
    );
  rxdat_3_MC_Q : X_BUF
    port map (
      I => rxdat_3_MC_Q_tsimrenamed_net_Q,
      O => rxdat_3_MC_Q_21
    );
  rxdat_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_rxdat_3_MC_REG_IN,
      CE => Vcc_25,
      CLK => NlwBufferSignal_rxdat_3_MC_REG_CLK,
      SET => Gnd_24,
      RST => Gnd_24,
      O => rxdat_3_MC_Q_tsimrenamed_net_Q
    );
  rxdat_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_rxdat_3_MC_D_IN0,
      I1 => NlwBufferSignal_rxdat_3_MC_D_IN1,
      O => rxdat_3_MC_D_123
    );
  rxdat_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_rxdat_3_MC_D1_IN0,
      I1 => NlwBufferSignal_rxdat_3_MC_D1_IN1,
      O => rxdat_3_MC_D1_124
    );
  rxdat_3_MC_D2 : X_ZERO
    port map (
      O => rxdat_3_MC_D2_125
    );
  NlwBufferBlock_ok_MC_REG_IN : X_BUF
    port map (
      I => ok_MC_D_23,
      O => NlwBufferSignal_ok_MC_REG_IN
    );
  NlwBufferBlock_ok_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_ok_MC_REG_CLK
    );
  NlwBufferBlock_ok_MC_D_IN0 : X_BUF
    port map (
      I => ok_MC_D1_26,
      O => NlwBufferSignal_ok_MC_D_IN0
    );
  NlwBufferBlock_ok_MC_D_IN1 : X_BUF
    port map (
      I => ok_MC_D2_27,
      O => NlwBufferSignal_ok_MC_D_IN1
    );
  NlwBufferBlock_ok_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D1_IN0
    );
  NlwBufferBlock_ok_MC_D1_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(0),
      O => NlwBufferSignal_ok_MC_D1_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_ok_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_ok_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_ok_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_ok_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_ok_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_ok_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_ok_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_ok_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_ok_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => pCheck_r1_cs(4),
      O => NlwBufferSignal_ok_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_ok_MC_D2_IN0 : X_BUF
    port map (
      I => ok_MC_D2_PT_0_33,
      O => NlwBufferSignal_ok_MC_D2_IN0
    );
  NlwBufferBlock_ok_MC_D2_IN1 : X_BUF
    port map (
      I => ok_MC_D2_PT_1_34,
      O => NlwBufferSignal_ok_MC_D2_IN1
    );
  NlwBufferBlock_ok_MC_D2_IN2 : X_BUF
    port map (
      I => ok_MC_D2_PT_2_35,
      O => NlwBufferSignal_ok_MC_D2_IN2
    );
  NlwBufferBlock_ok_MC_D2_IN3 : X_BUF
    port map (
      I => ok_MC_D2_PT_3_36,
      O => NlwBufferSignal_ok_MC_D2_IN3
    );
  NlwBufferBlock_ok_MC_D2_IN4 : X_BUF
    port map (
      I => ok_MC_D2_PT_4_37,
      O => NlwBufferSignal_ok_MC_D2_IN4
    );
  NlwBufferBlock_ok_MC_D2_IN5 : X_BUF
    port map (
      I => ok_MC_D2_PT_5_38,
      O => NlwBufferSignal_ok_MC_D2_IN5
    );
  NlwBufferBlock_ok_MC_D2_IN6 : X_BUF
    port map (
      I => ok_MC_D2_PT_6_39,
      O => NlwBufferSignal_ok_MC_D2_IN6
    );
  NlwBufferBlock_ok_MC_D2_IN7 : X_BUF
    port map (
      I => ok_MC_D2_PT_7_40,
      O => NlwBufferSignal_ok_MC_D2_IN7
    );
  NlwBufferBlock_pCheck_r1_cs_0_MC_REG_IN : X_BUF
    port map (
      I => pCheck_r1_cs_0_MC_D_42,
      O => NlwBufferSignal_pCheck_r1_cs_0_MC_REG_IN
    );
  NlwBufferBlock_pCheck_r1_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pCheck_r1_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_pCheck_r1_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => pCheck_r1_cs_0_MC_D1_43,
      O => NlwBufferSignal_pCheck_r1_cs_0_MC_D_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs_0_MC_D2_44,
      O => NlwBufferSignal_pCheck_r1_cs_0_MC_D_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pCheck_r1_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => bridge(0),
      O => NlwBufferSignal_pCheck_r1_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_bridge_0_MC_REG_IN : X_BUF
    port map (
      I => bridge_0_MC_D_47,
      O => NlwBufferSignal_bridge_0_MC_REG_IN
    );
  NlwBufferBlock_bridge_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_bridge_0_MC_REG_CLK
    );
  NlwBufferBlock_bridge_0_MC_D_IN0 : X_BUF
    port map (
      I => bridge_0_MC_D1_48,
      O => NlwBufferSignal_bridge_0_MC_D_IN0
    );
  NlwBufferBlock_bridge_0_MC_D_IN1 : X_BUF
    port map (
      I => bridge_0_MC_D2_49,
      O => NlwBufferSignal_bridge_0_MC_D_IN1
    );
  NlwBufferBlock_bridge_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_0_MC_D1_IN0
    );
  NlwBufferBlock_bridge_0_MC_D1_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(0),
      O => NlwBufferSignal_bridge_0_MC_D1_IN1
    );
  NlwBufferBlock_pGen_r1_cs_0_MC_REG_IN : X_BUF
    port map (
      I => pGen_r1_cs_0_MC_D_52,
      O => NlwBufferSignal_pGen_r1_cs_0_MC_REG_IN
    );
  NlwBufferBlock_pGen_r1_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pGen_r1_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_pGen_r1_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => pGen_r1_cs_0_MC_D1_53,
      O => NlwBufferSignal_pGen_r1_cs_0_MC_D_IN0
    );
  NlwBufferBlock_pGen_r1_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => pGen_r1_cs_0_MC_D2_54,
      O => NlwBufferSignal_pGen_r1_cs_0_MC_D_IN1
    );
  NlwBufferBlock_pGen_r1_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pGen_r1_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_pGen_r1_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => txdat_0_II_UIM_5,
      O => NlwBufferSignal_pGen_r1_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_1_MC_REG_IN : X_BUF
    port map (
      I => pCheck_r1_cs_1_MC_D_56,
      O => NlwBufferSignal_pCheck_r1_cs_1_MC_REG_IN
    );
  NlwBufferBlock_pCheck_r1_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pCheck_r1_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_pCheck_r1_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => pCheck_r1_cs_1_MC_D1_57,
      O => NlwBufferSignal_pCheck_r1_cs_1_MC_D_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs_1_MC_D2_58,
      O => NlwBufferSignal_pCheck_r1_cs_1_MC_D_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pCheck_r1_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => bridge(1),
      O => NlwBufferSignal_pCheck_r1_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_bridge_1_MC_REG_IN : X_BUF
    port map (
      I => bridge_1_MC_D_61,
      O => NlwBufferSignal_bridge_1_MC_REG_IN
    );
  NlwBufferBlock_bridge_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_bridge_1_MC_REG_CLK
    );
  NlwBufferBlock_bridge_1_MC_D_IN0 : X_BUF
    port map (
      I => bridge_1_MC_D1_62,
      O => NlwBufferSignal_bridge_1_MC_D_IN0
    );
  NlwBufferBlock_bridge_1_MC_D_IN1 : X_BUF
    port map (
      I => bridge_1_MC_D2_63,
      O => NlwBufferSignal_bridge_1_MC_D_IN1
    );
  NlwBufferBlock_bridge_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_1_MC_D1_IN0
    );
  NlwBufferBlock_bridge_1_MC_D1_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(1),
      O => NlwBufferSignal_bridge_1_MC_D1_IN1
    );
  NlwBufferBlock_pGen_r1_cs_1_MC_REG_IN : X_BUF
    port map (
      I => pGen_r1_cs_1_MC_D_66,
      O => NlwBufferSignal_pGen_r1_cs_1_MC_REG_IN
    );
  NlwBufferBlock_pGen_r1_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pGen_r1_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_pGen_r1_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => pGen_r1_cs_1_MC_D1_67,
      O => NlwBufferSignal_pGen_r1_cs_1_MC_D_IN0
    );
  NlwBufferBlock_pGen_r1_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => pGen_r1_cs_1_MC_D2_68,
      O => NlwBufferSignal_pGen_r1_cs_1_MC_D_IN1
    );
  NlwBufferBlock_pGen_r1_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pGen_r1_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_pGen_r1_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => txdat_1_II_UIM_7,
      O => NlwBufferSignal_pGen_r1_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_2_MC_REG_IN : X_BUF
    port map (
      I => pCheck_r1_cs_2_MC_D_70,
      O => NlwBufferSignal_pCheck_r1_cs_2_MC_REG_IN
    );
  NlwBufferBlock_pCheck_r1_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pCheck_r1_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_pCheck_r1_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => pCheck_r1_cs_2_MC_D1_71,
      O => NlwBufferSignal_pCheck_r1_cs_2_MC_D_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs_2_MC_D2_72,
      O => NlwBufferSignal_pCheck_r1_cs_2_MC_D_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pCheck_r1_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => bridge(2),
      O => NlwBufferSignal_pCheck_r1_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_bridge_2_MC_REG_IN : X_BUF
    port map (
      I => bridge_2_MC_D_75,
      O => NlwBufferSignal_bridge_2_MC_REG_IN
    );
  NlwBufferBlock_bridge_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_bridge_2_MC_REG_CLK
    );
  NlwBufferBlock_bridge_2_MC_D_IN0 : X_BUF
    port map (
      I => bridge_2_MC_D1_76,
      O => NlwBufferSignal_bridge_2_MC_D_IN0
    );
  NlwBufferBlock_bridge_2_MC_D_IN1 : X_BUF
    port map (
      I => bridge_2_MC_D2_77,
      O => NlwBufferSignal_bridge_2_MC_D_IN1
    );
  NlwBufferBlock_bridge_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_2_MC_D1_IN0
    );
  NlwBufferBlock_bridge_2_MC_D1_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(2),
      O => NlwBufferSignal_bridge_2_MC_D1_IN1
    );
  NlwBufferBlock_pGen_r1_cs_2_MC_REG_IN : X_BUF
    port map (
      I => pGen_r1_cs_2_MC_D_80,
      O => NlwBufferSignal_pGen_r1_cs_2_MC_REG_IN
    );
  NlwBufferBlock_pGen_r1_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pGen_r1_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_pGen_r1_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => pGen_r1_cs_2_MC_D1_81,
      O => NlwBufferSignal_pGen_r1_cs_2_MC_D_IN0
    );
  NlwBufferBlock_pGen_r1_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => pGen_r1_cs_2_MC_D2_82,
      O => NlwBufferSignal_pGen_r1_cs_2_MC_D_IN1
    );
  NlwBufferBlock_pGen_r1_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pGen_r1_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_pGen_r1_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => txdat_2_II_UIM_9,
      O => NlwBufferSignal_pGen_r1_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_3_MC_REG_IN : X_BUF
    port map (
      I => pCheck_r1_cs_3_MC_D_84,
      O => NlwBufferSignal_pCheck_r1_cs_3_MC_REG_IN
    );
  NlwBufferBlock_pCheck_r1_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pCheck_r1_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_pCheck_r1_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => pCheck_r1_cs_3_MC_D1_85,
      O => NlwBufferSignal_pCheck_r1_cs_3_MC_D_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs_3_MC_D2_86,
      O => NlwBufferSignal_pCheck_r1_cs_3_MC_D_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pCheck_r1_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => bridge(3),
      O => NlwBufferSignal_pCheck_r1_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_bridge_3_MC_REG_IN : X_BUF
    port map (
      I => bridge_3_MC_D_89,
      O => NlwBufferSignal_bridge_3_MC_REG_IN
    );
  NlwBufferBlock_bridge_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_bridge_3_MC_REG_CLK
    );
  NlwBufferBlock_bridge_3_MC_D_IN0 : X_BUF
    port map (
      I => bridge_3_MC_D1_90,
      O => NlwBufferSignal_bridge_3_MC_D_IN0
    );
  NlwBufferBlock_bridge_3_MC_D_IN1 : X_BUF
    port map (
      I => bridge_3_MC_D2_91,
      O => NlwBufferSignal_bridge_3_MC_D_IN1
    );
  NlwBufferBlock_bridge_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_3_MC_D1_IN0
    );
  NlwBufferBlock_bridge_3_MC_D1_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(3),
      O => NlwBufferSignal_bridge_3_MC_D1_IN1
    );
  NlwBufferBlock_pGen_r1_cs_3_MC_REG_IN : X_BUF
    port map (
      I => pGen_r1_cs_3_MC_D_94,
      O => NlwBufferSignal_pGen_r1_cs_3_MC_REG_IN
    );
  NlwBufferBlock_pGen_r1_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pGen_r1_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_pGen_r1_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => pGen_r1_cs_3_MC_D1_95,
      O => NlwBufferSignal_pGen_r1_cs_3_MC_D_IN0
    );
  NlwBufferBlock_pGen_r1_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => pGen_r1_cs_3_MC_D2_96,
      O => NlwBufferSignal_pGen_r1_cs_3_MC_D_IN1
    );
  NlwBufferBlock_pGen_r1_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pGen_r1_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_pGen_r1_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => txdat_3_II_UIM_11,
      O => NlwBufferSignal_pGen_r1_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_4_MC_REG_IN : X_BUF
    port map (
      I => pCheck_r1_cs_4_MC_D_98,
      O => NlwBufferSignal_pCheck_r1_cs_4_MC_REG_IN
    );
  NlwBufferBlock_pCheck_r1_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_pCheck_r1_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_pCheck_r1_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => pCheck_r1_cs_4_MC_D1_99,
      O => NlwBufferSignal_pCheck_r1_cs_4_MC_D_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs_4_MC_D2_100,
      O => NlwBufferSignal_pCheck_r1_cs_4_MC_D_IN1
    );
  NlwBufferBlock_pCheck_r1_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_pCheck_r1_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_pCheck_r1_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => bridge(4),
      O => NlwBufferSignal_pCheck_r1_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_bridge_4_MC_REG_IN : X_BUF
    port map (
      I => bridge_4_MC_D_103,
      O => NlwBufferSignal_bridge_4_MC_REG_IN
    );
  NlwBufferBlock_bridge_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_bridge_4_MC_REG_CLK
    );
  NlwBufferBlock_bridge_4_MC_D_IN0 : X_BUF
    port map (
      I => bridge_4_MC_D1_104,
      O => NlwBufferSignal_bridge_4_MC_D_IN0
    );
  NlwBufferBlock_bridge_4_MC_D_IN1 : X_BUF
    port map (
      I => bridge_4_MC_D2_105,
      O => NlwBufferSignal_bridge_4_MC_D_IN1
    );
  NlwBufferBlock_bridge_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_4_MC_D1_IN0
    );
  NlwBufferBlock_bridge_4_MC_D1_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(0),
      O => NlwBufferSignal_bridge_4_MC_D1_IN1
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(1),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => pGen_r1_cs(2),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => pGen_r1_cs(3),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(1),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => pGen_r1_cs(2),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => pGen_r1_cs(3),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(1),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => pGen_r1_cs(2),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => pGen_r1_cs(3),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => pGen_r1_cs(1),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => pGen_r1_cs(2),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_bridge_4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => pGen_r1_cs(3),
      O => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_bridge_4_MC_D2_IN0 : X_BUF
    port map (
      I => bridge_4_MC_D2_PT_0_106,
      O => NlwBufferSignal_bridge_4_MC_D2_IN0
    );
  NlwBufferBlock_bridge_4_MC_D2_IN1 : X_BUF
    port map (
      I => bridge_4_MC_D2_PT_1_107,
      O => NlwBufferSignal_bridge_4_MC_D2_IN1
    );
  NlwBufferBlock_bridge_4_MC_D2_IN2 : X_BUF
    port map (
      I => bridge_4_MC_D2_PT_2_108,
      O => NlwBufferSignal_bridge_4_MC_D2_IN2
    );
  NlwBufferBlock_bridge_4_MC_D2_IN3 : X_BUF
    port map (
      I => bridge_4_MC_D2_PT_3_109,
      O => NlwBufferSignal_bridge_4_MC_D2_IN3
    );
  NlwBufferBlock_rxdat_0_MC_REG_IN : X_BUF
    port map (
      I => rxdat_0_MC_D_111,
      O => NlwBufferSignal_rxdat_0_MC_REG_IN
    );
  NlwBufferBlock_rxdat_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_rxdat_0_MC_REG_CLK
    );
  NlwBufferBlock_rxdat_0_MC_D_IN0 : X_BUF
    port map (
      I => rxdat_0_MC_D1_112,
      O => NlwBufferSignal_rxdat_0_MC_D_IN0
    );
  NlwBufferBlock_rxdat_0_MC_D_IN1 : X_BUF
    port map (
      I => rxdat_0_MC_D2_113,
      O => NlwBufferSignal_rxdat_0_MC_D_IN1
    );
  NlwBufferBlock_rxdat_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_rxdat_0_MC_D1_IN0
    );
  NlwBufferBlock_rxdat_0_MC_D1_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(0),
      O => NlwBufferSignal_rxdat_0_MC_D1_IN1
    );
  NlwBufferBlock_rxdat_1_MC_REG_IN : X_BUF
    port map (
      I => rxdat_1_MC_D_115,
      O => NlwBufferSignal_rxdat_1_MC_REG_IN
    );
  NlwBufferBlock_rxdat_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_rxdat_1_MC_REG_CLK
    );
  NlwBufferBlock_rxdat_1_MC_D_IN0 : X_BUF
    port map (
      I => rxdat_1_MC_D1_116,
      O => NlwBufferSignal_rxdat_1_MC_D_IN0
    );
  NlwBufferBlock_rxdat_1_MC_D_IN1 : X_BUF
    port map (
      I => rxdat_1_MC_D2_117,
      O => NlwBufferSignal_rxdat_1_MC_D_IN1
    );
  NlwBufferBlock_rxdat_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_rxdat_1_MC_D1_IN0
    );
  NlwBufferBlock_rxdat_1_MC_D1_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(1),
      O => NlwBufferSignal_rxdat_1_MC_D1_IN1
    );
  NlwBufferBlock_rxdat_2_MC_REG_IN : X_BUF
    port map (
      I => rxdat_2_MC_D_119,
      O => NlwBufferSignal_rxdat_2_MC_REG_IN
    );
  NlwBufferBlock_rxdat_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_rxdat_2_MC_REG_CLK
    );
  NlwBufferBlock_rxdat_2_MC_D_IN0 : X_BUF
    port map (
      I => rxdat_2_MC_D1_120,
      O => NlwBufferSignal_rxdat_2_MC_D_IN0
    );
  NlwBufferBlock_rxdat_2_MC_D_IN1 : X_BUF
    port map (
      I => rxdat_2_MC_D2_121,
      O => NlwBufferSignal_rxdat_2_MC_D_IN1
    );
  NlwBufferBlock_rxdat_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_rxdat_2_MC_D1_IN0
    );
  NlwBufferBlock_rxdat_2_MC_D1_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(2),
      O => NlwBufferSignal_rxdat_2_MC_D1_IN1
    );
  NlwBufferBlock_rxdat_3_MC_REG_IN : X_BUF
    port map (
      I => rxdat_3_MC_D_123,
      O => NlwBufferSignal_rxdat_3_MC_REG_IN
    );
  NlwBufferBlock_rxdat_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_rxdat_3_MC_REG_CLK
    );
  NlwBufferBlock_rxdat_3_MC_D_IN0 : X_BUF
    port map (
      I => rxdat_3_MC_D1_124,
      O => NlwBufferSignal_rxdat_3_MC_D_IN0
    );
  NlwBufferBlock_rxdat_3_MC_D_IN1 : X_BUF
    port map (
      I => rxdat_3_MC_D2_125,
      O => NlwBufferSignal_rxdat_3_MC_D_IN1
    );
  NlwBufferBlock_rxdat_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_rxdat_3_MC_D1_IN0
    );
  NlwBufferBlock_rxdat_3_MC_D1_IN1 : X_BUF
    port map (
      I => pCheck_r1_cs(3),
      O => NlwBufferSignal_rxdat_3_MC_D1_IN1
    );
  NlwInverterBlock_ok_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_ok_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_ok_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_ok_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_ok_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_ok_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_ok_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_ok_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_ok_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_ok_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_ok_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_ok_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_ok_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_ok_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_ok_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_ok_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_ok_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_ok_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_ok_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_ok_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_ok_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_ok_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_ok_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_ok_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_ok_MC_D2_PT_6_IN4 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_6_IN4,
      O => NlwInverterSignal_ok_MC_D2_PT_6_IN4
    );
  NlwInverterBlock_ok_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_ok_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_ok_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_ok_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_ok_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_ok_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_ok_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_bridge_4_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_bridge_4_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_bridge_4_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_bridge_4_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_bridge_4_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_bridge_4_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_bridge_4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_bridge_4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_bridge_4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_bridge_4_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_bridge_4_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_bridge_4_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_bridge_4_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_bridge_4_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_bridge_4_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_bridge_4_MC_D2_PT_3_IN3
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

